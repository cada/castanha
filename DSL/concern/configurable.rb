module Configurable
  
  def self.with(*attrs)                 # `attrs` is created here
    # Define anonymous class with the configuration attributes
    config_class = Class.new do         # class definition passed in as a block
      attr_accessor *attrs              # we have access to `attrs` here
    end
    
    # Define anonymous module for the class methods to be "mixed in"
    class_methods = Module.new do       # define new module using a block
      define_method :config do          # method definition with a block
        @config ||= config_class.new    # even two blocks deep, we can still
      end  

      def configure
        yield config
      end
    end
 
    # Create and return new module
    Module.new do
      singleton_class.send :define_method, :included do |host_class|
        host_class.extend class_methods   # the block has access to `class_methods`
      end
    end
  end
  
end